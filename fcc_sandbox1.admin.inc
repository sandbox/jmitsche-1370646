<?php

/**
 * @File
 * Administrative callbacks for FCC Sandbox1
 */
 
function fcc_sandbox1_admin_setting(){
  $form = array();
  $options = array(
    0 => t('No Adverts'),
    1 => t('One Advert'),
    2 => t('Two Adverts'),
    1000 => t('1000 Adverts'),    
  );
  $form['number_of_frontpage_ads'] = array(
    '#type' => 'select',
    '#title' => t('Number of front page ads'),
    '#description' => t('Select how many ads are to be displayed on the front page'),
    '#options' => $options,
    '#default_value' => variable_get('number_of_frontpage_ads',0),
  );
  return system_settings_form($form);
}